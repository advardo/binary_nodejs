const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters) {
            throw Error('Can\t get fighters');
        }
        return fighters;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            throw Error('Fighter not found')
        }
        return item;
    }

    addFighter(fighter) {
        try {
            search(fighter.name);
            const fighterToAdd = FighterRepository.create(fighter);
            if (fighterToAdd) {
                return fighterToAdd;
            }
        } catch {
            
            throw Error('Can\'t add new fighter')
        }
        
    }


    deleteFighter(id) {
        const fighter = FighterRepository.delete(id);
        if(!fighter){
            throw Error('Fighter for delete not found');
        }
        return fighter;
    }
    
    updateFighter(id, data){
        const fighter = FighterRepository.update(id, data);
        if (fighter) {
            throw Error('Not found Fighter or fields to update');
        }
        return fighter;
    }

}

module.exports = new FighterService();