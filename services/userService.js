const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            throw Error('Can\t get users');
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('User not found')
        }
        return item;
    }

    addUser(user) {
        try {
            this.search(user.email);
            this.search(user.phoneNumber);
            const userToAdd = UserRepository.create(user);
            if (userToAdd) {
                return userToAdd;
            }   
        } catch {
            throw Error('Can\t add new User')
        }
        

    }


    deleteUser(id) {
        const user = UserRepository.delete(id);
        if(!user){
            throw Error('User for delete not found');
        }
        return user
    }
    
    updateUser(id, data){
        const user = UserRepository.update(id, data);
        if (user) {
            throw Error('Not found User or fields to update');
        }
        return user;
    }
}

module.exports = new UserService();