const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { getUsers, search, addUser, deleteUser, updateUser } = require('../services/userService');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next)=>{
    try {
        const users = getUsers();
        res.data = users;
    } catch(err){
        res.err = err;
        res.status(400);
    } finally {
        next()
    }
}, responseMiddleware);

router.get('/:id', (req, res, next)=>{
    try {
        const user = search(req.params);
        res.data = user;
    } catch (err){
        res.err = err;
        res.status(400);
    } finally {
        next()
    }

}, responseMiddleware);

router.post('/', createUserValid, (req, res, next)=>{
    try {
        const user = addUser(req.body);
        res.data = user;
    } catch (err){
        res.err = err;
        res.status(400);
    } finally {
        next()
    }

}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next)=>{
    try {
        const user = updateUser(req.params, req.body);
        res.data = user;
    } catch (err){
        res.err = err;
        res.status(400);
    } finally {
        next();
    }

}, responseMiddleware);

router.delete('/:id', (req, res, next)=>{
    try {
        const user = deleteUser(req.params);
        res.data = user;
    } catch (err){
        res.err = err;
        res.status(400);
    } finally {
        next();
    }
   
}, responseMiddleware);

module.exports = router;