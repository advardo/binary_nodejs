const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { getFighters, addFighter, updateFighter, deleteFighter, search } = require('../services/fighterService');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next)=>{
    try {
        const fighters = getFighters();
        res.data = fighters;
    } catch(err){
        res.err = err;
        res.status(400);
    } finally {
        next()
    }
}, responseMiddleware);

router.get('/:id', (req, res, next)=>{
    try {
        const fighter = search(req.params);
        res.data = fighter;
    } catch (err){
        res.err = err;
        res.status(400);
    } finally {
        next()
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next)=>{
    try {
        const fighter = addFighter(req.body);
        res.data = fighter;
    } catch (err){
        res.err = err;
        res.status(400);
    } finally {
        next()
    }

}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next)=>{
    try {
        const fighter = updateFighter(req.params, req.body);
        res.data = fighter;
    } catch (err){
        res.err = err;
        res.status(400);
    } finally {
        next();
    }

}, responseMiddleware);

router.delete('/:id', (req, res, next)=>{
    try {
        const user = deleteFighter(req.params);
        res.data = fighter;
    } catch (err){
        res.err = err;
        res.status(400);
    } finally {
        next();
    }
   
}, responseMiddleware);

module.exports = router;