const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {

    if (!Object.keys(fighter).filter(it => it != 'id' && it != 'health').every(elem => Object.keys(req.body).includes(elem)))
    {return res.json("Enter all fields")}

    if (req.body.id) {
        return res.json('Delete id from body')
    }

    const {name, lastName, power, defense} = req.body;

    if (parseInt(power) <1 && parseInt(power) > 100) {
        return res.json('Power must be 1-100');
    }
    
    if (parseInt(defense) <1 && parseInt(defense) > 10) {
        return res.json('Defense must be 1-10');
    }
    if (health) {
    if (parseInt(health) <80 && parseInt(health) > 120) {
        return res.json('Health must be 80-120');
    }}
    else { res.body.health = 100}

    // TODO: Implement validatior for fighter entity during creation
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    if (!Object.keys(req.body).filter(it => it != 'id' && it != 'health').every(elem => Object.keys(fighters).includes(elem)))
    {return res.json("Enter valid fields")}

    if (req.body.id) {
        return res.json('Delete id from body')
    }

    const {name, lastName, power, defense} = req.body;
    if (power) {
    if (parseInt(power) <1 && parseInt(power) > 100) {
        return res.json('Power must be 1-100');
    }}

    if (defense) {
    if (parseInt(defense) <1 && parseInt(defense) > 10) {
        return res.json('Defense must be 1-10');
    }}
    
    if (health) {
    if (parseInt(health) <80 && parseInt(health) > 120) {
        return res.json('Health must be 80-120');
    }}
    else { res.body.health = 100}


    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;