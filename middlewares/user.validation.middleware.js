const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    // 
    

    if (!Object.keys(user).filter(it => it != 'id').every(elem => Object.keys(req.body).includes(elem)))
    {return res.json("Enter all fields")}

    if (req.body.id) {
        return res.json('Delete id from body')
    }

    const {firstName, health, email, phoneNumber, password} = req.body;

    if (password.length <3) {
        return res.json('Password length must me more then 3 charachters');
    }
    if (!email.match(/@gmail.com$/g)) {  
        return res.json("Only @gmail.com");
    }
    if(!phoneNumber.match(/^[+380]\d+$/g) || phoneNumber.length != 13) { 
        return res.json('Only Ukrainian phone, starting from +380');
    }

    next();


}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if (!Object.keys(req.body).filter(it => it != 'id').every(elem => Object.keys(user).includes(elem)))
    {return res.json("Enter valid fields")}

    if (req.body.id) {
        return res.json('Delete id from body')
    }

    const {firstName, lastName, email, phoneNumber, password} = req.body;
    
    if (password){
    if (password.length <3) {
        return res.json('Password length must me more then 3 charachters');
    }}
    if (email){
    if (!email.match(/@gmail.com$/g)) {  
        return res.json("Only @gmail.com");
    }}
    if (phoneNumber) {
    if(!phoneNumber.match(/^[+380]\d+$/g) || phoneNumber.length != 13) { 
        return res.json('Only Ukrainian phone, starting from +380');
    }}


    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;