const e = require("express");

const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query

    if(!res.err){
        return res.status(200).send(res.data);
    }
    if(res.statusCode === 400) {
        res.json({ 
            error: true,
            message: res.err.message
        })
    }
    if ( res.statusCode === 404){
        res.json({
            error: true,
            mesasage: res.err.message
        })
    }


    next();
}

exports.responseMiddleware = responseMiddleware;